package com.prgant.his.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.prgant.his.model.po.Log;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface LogMapper extends BaseMapper<Log> {
}
