package com.prgant.his.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.prgant.his.model.po.DisposalRequest;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DisposalRequestMapper extends BaseMapper<DisposalRequest> {
}
