package com.prgant.his.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.prgant.his.model.po.DrugInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DrugInfoMapper extends BaseMapper<DrugInfo> {
}
