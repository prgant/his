package com.prgant.his.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("scheduling")
public class Scheduling {
    @TableId(type = IdType.AUTO,value = "id")
    private Integer id;
    private String ruleName;
    private String weekRule;
    private Integer delMark;

    public Scheduling() {
    }

    public Scheduling(Integer id, String ruleName, String weekRule, Integer delMark) {
        this.id = id;
        this.ruleName = ruleName;
        this.weekRule = weekRule;
        this.delMark = delMark;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRuleName() {
        return ruleName;
    }

    public void setRuleName(String ruleName) {
        this.ruleName = ruleName;
    }

    public String getWeekRule() {
        return weekRule;
    }

    public void setWeekRule(String weekRule) {
        this.weekRule = weekRule;
    }

    public Integer getDelMark() {
        return delMark;
    }

    public void setDelMark(Integer delMark) {
        this.delMark = delMark;
    }

    @Override
    public String toString() {
        return "Scheduling{" +
                "id=" + id +
                ", ruleName='" + ruleName + '\'' +
                ", weekRule='" + weekRule + '\'' +
                ", delMark=" + delMark +
                '}';
    }
}
