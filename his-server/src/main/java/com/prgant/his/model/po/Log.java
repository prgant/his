package com.prgant.his.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author prgant
 * @Description 系统日志表
 */
@TableName("log")
public class Log {
    @TableId(type = IdType.AUTO, value = "id")
    private Integer id;
    // 操作时间
    private String operTime;
    // 操作人
    private String operName;
    // 是否归档
    private String isFlag;
    // 职位
    private String dept;
    // 操作业务说明
    private String note;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getOperTime() {
        return operTime;
    }

    @Override
    public String toString() {
        return "Log{" +
                "id=" + id +
                ", operTime='" + operTime + '\'' +
                ", operName='" + operName + '\'' +
                ", isFlag='" + isFlag + '\'' +
                ", dept='" + dept + '\'' +
                ", note='" + note + '\'' +
                '}';
    }

    public void setOperTime(String operTime) {
        this.operTime = operTime;
    }

    public String getOperName() {
        return operName;
    }

    public void setOperName(String operName) {
        this.operName = operName;
    }

    public String getIsFlag() {
        return isFlag;
    }

    public void setIsFlag(String isFlag) {
        this.isFlag = isFlag;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Log() {
    }

    public Log(Integer id, String operTime, String operName, String isFlag, String dept, String note) {
        this.id = id;
        this.operTime = operTime;
        this.operName = operName;
        this.isFlag = isFlag;
        this.dept = dept;
        this.note = note;
    }
}
