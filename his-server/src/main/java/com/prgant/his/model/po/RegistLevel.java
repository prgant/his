package com.prgant.his.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("regist_level")
public class RegistLevel {
    @TableId(type = IdType.AUTO, value = "id")
    private Integer id;
    private String registCode;
    private String registName;
    private Double registFee;
    private Integer registQuota;
    private Integer delMark;
    private Integer sequenceNo;

    public RegistLevel() {}

    public RegistLevel(Integer id, String registCode, String registName, Double registFee, Integer registQuota, Integer delMark, Integer sequenceNo) {
        this.id = id;
        this.registCode = registCode;
        this.registName = registName;
        this.registFee = registFee;
        this.registQuota = registQuota;
        this.delMark = delMark;
        this.sequenceNo = sequenceNo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegistCode() {
        return registCode;
    }

    public void setRegistCode(String registCode) {
        this.registCode = registCode;
    }

    public String getRegistName() {
        return registName;
    }

    public void setRegistName(String registName) {
        this.registName = registName;
    }

    public Double getRegistFee() {
        return registFee;
    }

    public void setRegistFee(Double registFee) {
        this.registFee = registFee;
    }

    public Integer getRegistQuota() {
        return registQuota;
    }

    public void setRegistQuota(Integer registQuota) {
        this.registQuota = registQuota;
    }

    public Integer getDelMark() {
        return delMark;
    }

    public void setDelMark(Integer delMark) {
        this.delMark = delMark;
    }

    public Integer getSequenceNo() {
        return sequenceNo;
    }

    public void setSequenceNo(Integer sequenceNo) {
        this.sequenceNo = sequenceNo;
    }

    @Override
    public String toString() {
        return "RegistLevel{" +
                "id=" + id +
                ", registCode='" + registCode + '\'' +
                ", registName='" + registName + '\'' +
                ", registFee=" + registFee +
                ", registQuota=" + registQuota +
                ", delMark=" + delMark +
                ", sequenceNo=" + sequenceNo +
                '}';
    }
}
