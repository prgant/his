package com.prgant.his.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

@TableName("disposal_request")
public class DisposalRequest {
    @TableId(type = IdType.AUTO, value = "id")
    private Integer id;
    private Integer registerId;
    private Integer medicalTechnologyId;
    private String disposalInfo;
    private String disposalPosition;
    private Date creationTime;
    private Integer disposalEmployeeId;
    private Integer inputdisposalEmployeeId;
    private String proposal;
    private String careful;
    private String diagnosis;
    private String cure;

    public DisposalRequest() {
    }

    public DisposalRequest(Integer id, Integer registerId, Integer medicalTechnologyId, String disposalInfo, String disposalPosition, Date creationTime, Integer disposalEmployeeId, Integer inputdisposalEmployeeId, String proposal, String careful, String diagnosis, String cure) {
        this.id = id;
        this.registerId = registerId;
        this.medicalTechnologyId = medicalTechnologyId;
        this.disposalInfo = disposalInfo;
        this.disposalPosition = disposalPosition;
        this.creationTime = creationTime;
    }

    @Override
    public String toString() {
        return "DisposalRequest{" +
                "id=" + id +
                ", registerId=" + registerId +
                ", medicalTechnologyId=" + medicalTechnologyId +
                ", disposalInfo='" + disposalInfo + '\'' +
                ", disposalPosition='" + disposalPosition + '\'' +
                ", creationTime=" + creationTime +
                ", disposalEmployeeId=" + disposalEmployeeId +
                ", inputdisposalEmployeeId=" + inputdisposalEmployeeId +
                ", proposal='" + proposal + '\'' +
                ", careful='" + careful + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", cure='" + cure + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public Integer getMedicalTechnologyId() {
        return medicalTechnologyId;
    }

    public void setMedicalTechnologyId(Integer medicalTechnologyId) {
        this.medicalTechnologyId = medicalTechnologyId;
    }

    public String getDisposalInfo() {
        return disposalInfo;
    }

    public void setDisposalInfo(String disposalInfo) {
        this.disposalInfo = disposalInfo;
    }

    public String getDisposalPosition() {
        return disposalPosition;
    }

    public void setDisposalPosition(String disposalPosition) {
        this.disposalPosition = disposalPosition;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Integer getDisposalEmployeeId() {
        return disposalEmployeeId;
    }

    public void setDisposalEmployeeId(Integer disposalEmployeeId) {
        this.disposalEmployeeId = disposalEmployeeId;
    }

    public Integer getInputdisposalEmployeeId() {
        return inputdisposalEmployeeId;
    }

    public void setInputdisposalEmployeeId(Integer inputdisposalEmployeeId) {
        this.inputdisposalEmployeeId = inputdisposalEmployeeId;
    }

    public String getProposal() {
        return proposal;
    }

    public void setProposal(String proposal) {
        this.proposal = proposal;
    }

    public String getCareful() {
        return careful;
    }

    public void setCareful(String careful) {
        this.careful = careful;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getCure() {
        return cure;
    }

    public void setCure(String cure) {
        this.cure = cure;
    }
}
