package com.prgant.his.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

@TableName("check_request")
public class CheckRequest {
    @TableId(type = IdType.AUTO, value = "id")
    private Integer id;
    private Integer medicalTechnologyId;
    private Integer registerId;
    private String checkInfo;
    private String checkPosition;
    private String creationTime;
    private Integer checkEmployeeId;
    private Integer inputcheckEmployeeId;
    private Date checkTime;
    private String checkResult;
    private String checkState;
    private String checkRemark;

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "CheckRequest{" +
                "id=" + id +
                ", medicalTechnologyId=" + medicalTechnologyId +
                ", registerId=" + registerId +
                ", checkInfo='" + checkInfo + '\'' +
                ", checkPosition='" + checkPosition + '\'' +
                ", creationTime='" + creationTime + '\'' +
                ", checkEmployeeId=" + checkEmployeeId +
                ", inputcheckEmployeeId=" + inputcheckEmployeeId +
                ", checkTime=" + checkTime +
                ", checkResult='" + checkResult + '\'' +
                ", checkState='" + checkState + '\'' +
                ", checkRemark='" + checkRemark + '\'' +
                '}';
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMedicalTechnologyId() {
        return medicalTechnologyId;
    }

    public void setMedicalTechnologyId(Integer medicalTechnologyId) {
        this.medicalTechnologyId = medicalTechnologyId;
    }

    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public String getCheckInfo() {
        return checkInfo;
    }

    public void setCheckInfo(String checkInfo) {
        this.checkInfo = checkInfo;
    }

    public String getCheckPosition() {
        return checkPosition;
    }

    public void setCheckPosition(String checkPosition) {
        this.checkPosition = checkPosition;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public Integer getCheckEmployeeId() {
        return checkEmployeeId;
    }

    public void setCheckEmployeeId(Integer checkEmployeeId) {
        this.checkEmployeeId = checkEmployeeId;
    }

    public Integer getInputcheckEmployeeId() {
        return inputcheckEmployeeId;
    }

    public void setInputcheckEmployeeId(Integer inputcheckEmployeeId) {
        this.inputcheckEmployeeId = inputcheckEmployeeId;
    }

    public Date getCheckTime() {
        return checkTime;
    }

    public void setCheckTime(Date checkTime) {
        this.checkTime = checkTime;
    }

    public String getCheckResult() {
        return checkResult;
    }

    public void setCheckResult(String checkResult) {
        this.checkResult = checkResult;
    }

    public String getCheckState() {
        return checkState;
    }

    public void setCheckState(String checkState) {
        this.checkState = checkState;
    }

    public String getCheckRemark() {
        return checkRemark;
    }

    public void setCheckRemark(String checkRemark) {
        this.checkRemark = checkRemark;
    }

    public CheckRequest(Integer id, Integer medicalTechnologyId, Integer registerId, String checkInfo, String checkPosition, String creationTime, Integer checkEmployeeId, Integer inputcheckEmployeeId, Date checkTime, String checkResult, String checkState, String checkRemark) {
        this.id = id;
        this.medicalTechnologyId = medicalTechnologyId;
        this.registerId = registerId;
        this.checkInfo = checkInfo;
        this.checkPosition = checkPosition;
        this.creationTime = creationTime;
        this.checkEmployeeId = checkEmployeeId;
        this.inputcheckEmployeeId = inputcheckEmployeeId;
        this.checkTime = checkTime;
        this.checkResult = checkResult;
        this.checkState = checkState;
        this.checkRemark = checkRemark;
    }

    public CheckRequest() {
    }
}
