package com.prgant.his.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("employee")
public class Employee {
    @TableId(type = IdType.AUTO,value = "id")
    private Integer id;
    private Integer registLevelId;
    private Integer deptmentId;
    private Integer schedulingId;
    private String realname;
    private String password;
    private Integer delMark;

    public Employee() {}

    public Employee(Integer id, Integer registLevelId, Integer departmentId, Integer schedulingId, String realname, String password, Integer delMark) {
        this.id = id;
        this.registLevelId = registLevelId;
        this.deptmentId = departmentId;
        this.schedulingId = schedulingId;
        this.realname = realname;
        this.password = password;
        this.delMark = delMark;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRegistLevelId() {
        return registLevelId;
    }

    public void setRegistLevelId(Integer registLevelId) {
        this.registLevelId = registLevelId;
    }

    public Integer getDepartmentId() {
        return deptmentId;
    }

    public void setDepartmentId(Integer departmentId) {
        this.deptmentId = departmentId;
    }

    public Integer getSchedulingId() {
        return schedulingId;
    }

    public void setSchedulingId(Integer schedulingId) {
        this.schedulingId = schedulingId;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getDelMark() {
        return delMark;
    }

    public void setDelMark(Integer delMark) {
        this.delMark = delMark;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", registLevelId=" + registLevelId +
                ", deptmentId=" + deptmentId +
                ", schedulingId=" + schedulingId +
                ", realname='" + realname + '\'' +
                ", password='" + password + '\'' +
                ", delMark=" + delMark +
                '}';
    }
}
