package com.prgant.his.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author prgant
 * @Description 药品信息
 */
@TableName("drug_info")
public class DrugInfo {
    @TableId(type = IdType.AUTO, value = "id")
    private Integer id;
    // 药品编码
    private String drugCode;
    // 药品名称
    private String drugName;
    // 药品规格
    private String drugFormat;
    // 包装单位
    private String drugUnit;
    // 生产厂家
    private String manufacturer;
    // 药剂类型
    private String drugDosage;
    // 药品类型
    private String drugType;
    // 药品单价
    private String drugPrice;
    // 拼音助记码
    private String mnemonicCode;
    // 创建时间
    private String creationDate;
}
