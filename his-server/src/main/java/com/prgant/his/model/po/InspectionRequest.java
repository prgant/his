package com.prgant.his.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

@TableName("inspection_request")
public class InspectionRequest {
    @TableId(type = IdType.AUTO,value = "id")
    private Integer id;
    private Integer registerId;
    private Integer medicalTechnologyId;
    private String inspectionInfo;
    private String inspectionPosition;
    private Date creationTime;
    private Integer inspectionEmployeeId;
    private Integer inputinspectionEmployeeId;
    private Date inspectionTime;
    private String inspectionResult;
    private String inspectionState;
    private String  inspectionRemark;
    public InspectionRequest(){}

    @Override
    public String toString() {
        return "InspectionRequest{" +
                "id=" + id +
                ", registerId=" + registerId +
                ", medicalTechnologyId=" + medicalTechnologyId +
                ", inspectionInfo='" + inspectionInfo + '\'' +
                ", inspectionPosition='" + inspectionPosition + '\'' +
                ", creationTime=" + creationTime +
                ", inspectionEmployeeId=" + inspectionEmployeeId +
                ", inputinspectionEmployeeId=" + inputinspectionEmployeeId +
                ", inspectionTime=" + inspectionTime +
                ", inspectionResult='" + inspectionResult + '\'' +
                ", inspectionState='" + inspectionState + '\'' +
                ", inspectionRemark='" + inspectionRemark + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public Integer getMedicalTechnologyId() {
        return medicalTechnologyId;
    }

    public void setMedicalTechnologyId(Integer medicalTechnologyId) {
        this.medicalTechnologyId = medicalTechnologyId;
    }

    public String getInspectionInfo() {
        return inspectionInfo;
    }

    public void setInspectionInfo(String inspectionInfo) {
        this.inspectionInfo = inspectionInfo;
    }

    public String getInspectionPosition() {
        return inspectionPosition;
    }

    public void setInspectionPosition(String inspectionPosition) {
        this.inspectionPosition = inspectionPosition;
    }

    public Date getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Integer getInspectionEmployeeId() {
        return inspectionEmployeeId;
    }

    public void setInspectionEmployeeId(Integer inspectionEmployeeId) {
        this.inspectionEmployeeId = inspectionEmployeeId;
    }

    public Integer getInputinspectionEmployeeId() {
        return inputinspectionEmployeeId;
    }

    public void setInputinspectionEmployeeId(Integer inputinspectionEmployeeId) {
        this.inputinspectionEmployeeId = inputinspectionEmployeeId;
    }

    public Date getInspectionTime() {
        return inspectionTime;
    }

    public void setInspectionTime(Date inspectionTime) {
        this.inspectionTime = inspectionTime;
    }

    public String getInspectionResult() {
        return inspectionResult;
    }

    public void setInspectionResult(String inspectionResult) {
        this.inspectionResult = inspectionResult;
    }

    public String getInspectionState() {
        return inspectionState;
    }

    public void setInspectionState(String inspectionState) {
        this.inspectionState = inspectionState;
    }

    public String getInspectionRemark() {
        return inspectionRemark;
    }

    public void setInspectionRemark(String inspectionRemark) {
        this.inspectionRemark = inspectionRemark;
    }

    public InspectionRequest(Integer id, Integer registerId, Integer medicalTechnologyId, String inspectionInfo, String inspectionPosition, Date creationTime, Integer inspectionEmployeeId, Integer inputinspectionEmployeeId, Date inspectionTime, String inspectionResult, String inspectionState, String inspectionRemark) {
        this.id = id;
        this.registerId = registerId;
        this.medicalTechnologyId = medicalTechnologyId;
        this.inspectionInfo = inspectionInfo;
        this.inspectionPosition = inspectionPosition;
        this.creationTime = creationTime;
        this.inspectionEmployeeId = inspectionEmployeeId;
        this.inputinspectionEmployeeId = inputinspectionEmployeeId;
        this.inspectionTime = inspectionTime;
        this.inspectionResult = inspectionResult;
        this.inspectionState = inspectionState;
        this.inspectionRemark = inspectionRemark;
    }
}
