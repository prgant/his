package com.prgant.his.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @description: 医技项目表
 * @author prgant
 */
@TableName("medical_technology")
public class MedicalTechnology {
    @TableId(type = IdType.AUTO,value = "id")
    private Integer id;

    //项目编码
    private String techCode;
    // 项目名称
    private String techName;
    // 规格
    private String techFormat;
    // 单价
    private Double techPrice;
    // 类型
    private String techType;
    // 费用分类
    private String priceType;
    // 执行科室
    private Integer deptmentId;

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        return "MedicalTechnology{" +
                "id=" + id +
                ", techCode='" + techCode + '\'' +
                ", techName='" + techName + '\'' +
                ", techFormat='" + techFormat + '\'' +
                ", techPrice=" + techPrice +
                ", techType='" + techType + '\'' +
                ", priceType='" + priceType + '\'' +
                ", deptmentId=" + deptmentId +
                '}';
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTechCode() {
        return techCode;
    }

    public void setTechCode(String techCode) {
        this.techCode = techCode;
    }

    public String getTechName() {
        return techName;
    }

    public void setTechName(String techName) {
        this.techName = techName;
    }

    public String getTechFormat() {
        return techFormat;
    }

    public void setTechFormat(String techFormat) {
        this.techFormat = techFormat;
    }

    public Double getTechPrice() {
        return techPrice;
    }

    public void setTechPrice(Double techPrice) {
        this.techPrice = techPrice;
    }

    public String getTechType() {
        return techType;
    }

    public void setTechType(String techType) {
        this.techType = techType;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public Integer getDeptmentId() {
        return deptmentId;
    }

    public void setDeptmentId(Integer deptmentId) {
        this.deptmentId = deptmentId;
    }

    public MedicalTechnology(Integer id, String techCode, String techName, String techFormat, Double techPrice, String techType, String priceType, Integer deptmentId) {
        this.id = id;
        this.techCode = techCode;
        this.techName = techName;
        this.techFormat = techFormat;
        this.techPrice = techPrice;
        this.techType = techType;
        this.priceType = priceType;
        this.deptmentId = deptmentId;
    }

    public MedicalTechnology() {
   }
}
