package com.prgant.his.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.List;

/**
 * @description: 患者病历表
 * @author prgant
 */
@TableName("medical_record")
public class MedicalRecord {
    @TableId(type = IdType.AUTO,value = "id")
    private Integer id;
    // 挂号ID
    private Integer registerId;
    // 主诉
    private String readme;
    // 现病史
    private String present;
    // 现病治疗情况
    private String presentTreat;
    // 既往史
    private String history;
    // 过敏史
    private String allergy;
    // 体格检查
    private String physique;
    // 检查/检验建议
    private String proposal;
    // 注意事项
    private String careful;
    // 诊断结果
    private String diagnosis;
    // 处理意见
    private String cure;

    public List<Disease> getDiseases() {
        return diseases;
    }

    @Override
    public String toString() {
        return "MedicalRecord{" +
                "id=" + id +
                ", registerId=" + registerId +
                ", readme='" + readme + '\'' +
                ", present='" + present + '\'' +
                ", presentTreat='" + presentTreat + '\'' +
                ", history='" + history + '\'' +
                ", allergy='" + allergy + '\'' +
                ", physique='" + physique + '\'' +
                ", proposal='" + proposal + '\'' +
                ", careful='" + careful + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", cure='" + cure + '\'' +
                ", diseases=" + diseases +
                '}';
    }

    public void setDiseases(List<Disease> diseases) {
        this.diseases = diseases;
    }

    // 关联疾病
    private List<Disease> diseases;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public String getReadme() {
        return readme;
    }

    public void setReadme(String readme) {
        this.readme = readme;
    }

    public String getPresent() {
        return present;
    }

    public void setPresent(String present) {
        this.present = present;
    }

    public String getPresentTreat() {
        return presentTreat;
    }

    public void setPresentTreat(String presentTreat) {
        this.presentTreat = presentTreat;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getAllergy() {
        return allergy;
    }

    public void setAllergy(String allergy) {
        this.allergy = allergy;
    }

    public String getPhysique() {
        return physique;
    }

    public void setPhysique(String physique) {
        this.physique = physique;
    }

    public String getProposal() {
        return proposal;
    }

    public void setProposal(String proposal) {
        this.proposal = proposal;
    }

    public String getCareful() {
        return careful;
    }

    public void setCareful(String careful) {
        this.careful = careful;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getCure() {
        return cure;
    }

    public void setCure(String cure) {
        this.cure = cure;
    }

    public MedicalRecord() {
    }

    public MedicalRecord(Integer id, Integer registerId, String readme, String present, String presentTreat, String history, String allergy, String physique, String proposal, String careful, String diagnosis, String cure) {
        this.id = id;
        this.registerId = registerId;
        this.readme = readme;
        this.present = present;
        this.presentTreat = presentTreat;
        this.history = history;
        this.allergy = allergy;
        this.physique = physique;
        this.proposal = proposal;
        this.careful = careful;
        this.diagnosis = diagnosis;
        this.cure = cure;
    }
}
