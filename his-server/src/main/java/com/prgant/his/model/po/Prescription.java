package com.prgant.his.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author prgant
 * @date 2024/07/19
 * @description: 处方
 */
@TableName("prescription")
public class Prescription {
    @TableId(type = IdType.AUTO, value = "id")
    private Integer id;
    // 病历ID
    private Integer registerId;

    // 药品ID
    private Integer drugId;

    // 用法用量批次
    private String drugUsage;
    // 数量
    private Integer drugNumber;
    // 开立时间
    private String creationTime;

    @Override
    public String toString() {
        return "Prescription{" +
                "id=" + id +
                ", registerId=" + registerId +
                ", drugId=" + drugId +
                ", drugUsage='" + drugUsage + '\'' +
                ", drugNumber=" + drugNumber +
                ", creationTime='" + creationTime + '\'' +
                ", drugState='" + drugState + '\'' +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRegisterId() {
        return registerId;
    }

    public void setRegisterId(Integer registerId) {
        this.registerId = registerId;
    }

    public Integer getDrugId() {
        return drugId;
    }

    public void setDrugId(Integer drugId) {
        this.drugId = drugId;
    }

    public String getDrugUsage() {
        return drugUsage;
    }

    public void setDrugUsage(String drugUsage) {
        this.drugUsage = drugUsage;
    }

    public Integer getDrugNumber() {
        return drugNumber;
    }

    public void setDrugNumber(Integer drugNumber) {
        this.drugNumber = drugNumber;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getDrugState() {
        return drugState;
    }

    public void setDrugState(String drugState) {
        this.drugState = drugState;
    }

    public Prescription() {
    }

    // 状态
    private String drugState;

    public Prescription(Integer id, Integer registerId, Integer drugId, String drugUsage, Integer drugNumber, String creationTime, String drugState) {
        this.id = id;
        this.registerId = registerId;
        this.drugId = drugId;
        this.drugUsage = drugUsage;
        this.drugNumber = drugNumber;
        this.creationTime = creationTime;
        this.drugState = drugState;
    }
}
