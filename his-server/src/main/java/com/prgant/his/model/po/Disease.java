package com.prgant.his.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import jdk.dynalink.linker.LinkerServices;

import java.util.List;

/**
 * @author prgant
 * @date 2020/4/16
 * @description: 疾病
 */
@TableName("disease")
public class Disease {
    @TableId(type = IdType.AUTO, value = "id")
    private Integer id;
    // 疾病助记编码
    private String diseaseCode;
    // 疾病名称
    private String diseaseName;
    // 国际ICD编码
    private String diseaseICD;
    // 疾病分类
    private String diseaseCategory;

    // 关联病历
    private List<MedicalRecord> medicalRecords;

    public List<MedicalRecord> getMedicalRecords() {
        return medicalRecords;
    }

    public void setMedicalRecords(List<MedicalRecord> medicalRecords) {
        this.medicalRecords = medicalRecords;
    }

    @Override
    public String toString() {
        return "Disease{" +
                "id=" + id +
                ", diseaseCode='" + diseaseCode + '\'' +
                ", diseaseName='" + diseaseName + '\'' +
                ", diseaseICD='" + diseaseICD + '\'' +
                ", diseaseCategory='" + diseaseCategory + '\'' +
                ", medicalRecords=" + medicalRecords +
                '}';
    }

    public Disease(Integer id, String diseaseCode, String diseaseName, String diseaseICD, String diseaseCategory) {
        this.id = id;
        this.diseaseCode = diseaseCode;
        this.diseaseName = diseaseName;
        this.diseaseICD = diseaseICD;
        this.diseaseCategory = diseaseCategory;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDiseaseCode() {
        return diseaseCode;
    }

    public void setDiseaseCode(String diseaseCode) {
        this.diseaseCode = diseaseCode;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public String getDiseaseICD() {
        return diseaseICD;
    }

    public void setDiseaseICD(String diseaseICD) {
        this.diseaseICD = diseaseICD;
    }

    public String getDiseaseCategory() {
        return diseaseCategory;
    }

    public void setDiseaseCategory(String diseaseCategory) {
        this.diseaseCategory = diseaseCategory;
    }

    public Disease() {
    }
}
