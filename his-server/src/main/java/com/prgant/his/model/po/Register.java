package com.prgant.his.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Date;

@TableName("register")
public class Register {
    @TableId(type = IdType.AUTO,value = "id")
    private Integer id;

    private Integer caseNumber;
    private Integer realname;
    private String gender;
    private String cardNumber;
    private String birthdate;
    private Integer age;
    private String ageType;
    private String homeAddress;
    private Date visitDate;
    private String noon;
    private Integer deptmentId;
    private Integer registLevelId;
    private Integer employeeId;
    private Integer settleCategoryId;
    private String isBook;
    private String registMethod;
    private Double registMoney;
    private Integer visitState;

    public Register() {
    }

    public Register(Integer id, Integer caseNumber, Integer realname, String gender, String cardNumber, String birthdate, Integer age, String ageType, String homeAddress, Date visitDate, String noon, Integer deptmentId, Integer registLevelId, Integer employeeId, Integer settleCategoryId, String isBook, String registMethod, Double registMoney, Integer visitState) {
        this.id = id;
        this.caseNumber = caseNumber;
        this.realname = realname;
        this.gender = gender;
        this.cardNumber = cardNumber;
        this.birthdate = birthdate;
        this.age = age;
        this.ageType = ageType;
        this.homeAddress = homeAddress;
        this.visitDate = visitDate;
        this.noon = noon;
        this.deptmentId = deptmentId;
        this.registLevelId = registLevelId;
        this.employeeId = employeeId;
        this.settleCategoryId = settleCategoryId;
        this.isBook = isBook;
        this.registMethod = registMethod;
        this.registMoney = registMoney;
        this.visitState = visitState;
    }

    @Override
    public String toString() {
        return "Register{" +
                "id=" + id +
                ", caseNumber=" + caseNumber +
                ", realname=" + realname +
                ", gender='" + gender + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", birthdate='" + birthdate + '\'' +
                ", age=" + age +
                ", ageType='" + ageType + '\'' +
                ", homeAddress='" + homeAddress + '\'' +
                ", visitDate=" + visitDate +
                ", noon='" + noon + '\'' +
                ", deptmentId=" + deptmentId +
                ", registLevelId=" + registLevelId +
                ", employeeId=" + employeeId +
                ", settleCategoryId=" + settleCategoryId +
                ", isBook='" + isBook + '\'' +
                ", registMethod='" + registMethod + '\'' +
                ", registMoney=" + registMoney +
                ", visitState=" + visitState +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCaseNumber() {
        return caseNumber;
    }

    public void setCaseNumber(Integer caseNumber) {
        this.caseNumber = caseNumber;
    }

    public Integer getRealname() {
        return realname;
    }

    public void setRealname(Integer realname) {
        this.realname = realname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getAgeType() {
        return ageType;
    }

    public void setAgeType(String ageType) {
        this.ageType = ageType;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public String getNoon() {
        return noon;
    }

    public void setNoon(String noon) {
        this.noon = noon;
    }

    public Integer getDeptmentId() {
        return deptmentId;
    }

    public void setDeptmentId(Integer deptmentId) {
        this.deptmentId = deptmentId;
    }

    public Integer getRegistLevelId() {
        return registLevelId;
    }

    public void setRegistLevelId(Integer registLevelId) {
        this.registLevelId = registLevelId;
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public Integer getSettleCategoryId() {
        return settleCategoryId;
    }

    public void setSettleCategoryId(Integer settleCategoryId) {
        this.settleCategoryId = settleCategoryId;
    }

    public String getIsBook() {
        return isBook;
    }

    public void setIsBook(String isBook) {
        this.isBook = isBook;
    }

    public String getRegistMethod() {
        return registMethod;
    }

    public void setRegistMethod(String registMethod) {
        this.registMethod = registMethod;
    }

    public Double getRegistMoney() {
        return registMoney;
    }

    public void setRegistMoney(Double registMoney) {
        this.registMoney = registMoney;
    }

    public Integer getVisitState() {
        return visitState;
    }

    public void setVisitState(Integer visitState) {
        this.visitState = visitState;
    }
}
