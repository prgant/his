package com.prgant.his.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prgant.his.mapper.InspectionRequestMapper;
import com.prgant.his.model.po.InspectionRequest;
import com.prgant.his.service.InspectionRequestService;
import org.springframework.stereotype.Service;

@Service
public class InspectionRequestServiceImpl extends ServiceImpl<InspectionRequestMapper, InspectionRequest>
        implements InspectionRequestService {
}
