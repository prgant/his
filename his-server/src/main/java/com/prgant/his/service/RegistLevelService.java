package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.RegistLevel;

public interface RegistLevelService extends IService<RegistLevel> {
}
