package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.Scheduling;

public interface SchedulingService extends IService<Scheduling> {
}
