package com.prgant.his.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prgant.his.mapper.EmployeeMapper;
import com.prgant.his.model.po.Employee;
import com.prgant.his.service.EmployeeService;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public class EmployeeServiceImpl extends ServiceImpl<EmployeeMapper, Employee> implements EmployeeService {
}
