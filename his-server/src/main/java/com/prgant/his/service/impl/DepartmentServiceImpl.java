package com.prgant.his.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prgant.his.mapper.DepartmentMapper;
import com.prgant.his.model.po.Department;
import com.prgant.his.service.DepartmentService;
import org.springframework.stereotype.Service;

@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements DepartmentService {
}
