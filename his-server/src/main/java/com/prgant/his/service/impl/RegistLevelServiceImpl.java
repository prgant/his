package com.prgant.his.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prgant.his.mapper.RegistLevelMapper;
import com.prgant.his.model.po.RegistLevel;
import com.prgant.his.service.RegistLevelService;
import org.springframework.stereotype.Service;

@Service
public class RegistLevelServiceImpl extends ServiceImpl<RegistLevelMapper, RegistLevel> implements RegistLevelService {
}
