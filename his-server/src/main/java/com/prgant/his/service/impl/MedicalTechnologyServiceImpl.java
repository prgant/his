package com.prgant.his.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prgant.his.mapper.MedicalTechnologyMapper;
import com.prgant.his.model.po.MedicalTechnology;
import com.prgant.his.service.MedicalTechnologyService;
import org.springframework.stereotype.Service;

@Service
public class MedicalTechnologyServiceImpl extends ServiceImpl<MedicalTechnologyMapper, MedicalTechnology>
        implements MedicalTechnologyService {
}
