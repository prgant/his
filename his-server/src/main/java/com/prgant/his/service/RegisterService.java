package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.Register;

public interface RegisterService extends IService<Register> {
}
