package com.prgant.his.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prgant.his.mapper.PrescriptionMapper;
import com.prgant.his.model.po.Prescription;
import com.prgant.his.service.PrescriptionService;
import org.springframework.stereotype.Service;

@Service
public class PrescriptionServiceImpl extends ServiceImpl<PrescriptionMapper, Prescription> implements PrescriptionService {
}
