package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.Prescription;

public interface PrescriptionService extends IService<Prescription> {
}
