package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.MedicalTechnology;

public interface MedicalTechnologyService extends IService<MedicalTechnology> {
}
