package com.prgant.his.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prgant.his.mapper.SchedulingMapper;
import com.prgant.his.model.po.Scheduling;
import com.prgant.his.service.SchedulingService;
import org.springframework.stereotype.Service;

@Service
public class SchedulingServiceImpl extends ServiceImpl<SchedulingMapper, Scheduling> implements SchedulingService {
}
