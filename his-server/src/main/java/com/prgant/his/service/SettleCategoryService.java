package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.SettleCategory;

public interface SettleCategoryService extends IService<SettleCategory> {
}
