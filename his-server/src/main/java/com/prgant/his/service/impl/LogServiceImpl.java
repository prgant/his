package com.prgant.his.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prgant.his.mapper.LogMapper;
import com.prgant.his.model.po.Log;
import com.prgant.his.service.LogService;
import org.springframework.stereotype.Service;

@Service
public class LogServiceImpl extends ServiceImpl<LogMapper, Log> implements LogService {
}
