package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.MedicalRecord;

public interface MedicalRecordService extends IService<MedicalRecord> {
}
