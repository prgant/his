package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.DisposalRequest;

public interface DisposalRequestService extends IService<DisposalRequest> {
}
