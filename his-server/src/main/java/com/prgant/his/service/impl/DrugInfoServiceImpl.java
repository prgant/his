package com.prgant.his.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prgant.his.mapper.DrugInfoMapper;
import com.prgant.his.model.po.DrugInfo;
import com.prgant.his.service.DrugInfoService;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public class DrugInfoServiceImpl extends ServiceImpl<DrugInfoMapper, DrugInfo> implements DrugInfoService {
}
