package com.prgant.his.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prgant.his.mapper.MedicalRecordMapper;
import com.prgant.his.model.po.MedicalRecord;
import com.prgant.his.service.MedicalRecordService;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public class MedicalRecordServiceImpl extends ServiceImpl<MedicalRecordMapper, MedicalRecord>
        implements MedicalRecordService {
}
