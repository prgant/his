package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.CheckRequest;

public interface CheckRequestService extends IService<CheckRequest> {
}
