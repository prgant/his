package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.Disease;

public interface DiseaseService extends IService<Disease> {
}
