package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.Department;

public interface DepartmentService extends IService<Department> {
}
