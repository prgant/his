package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.Employee;

public interface EmployeeService extends IService<Employee> {
}
