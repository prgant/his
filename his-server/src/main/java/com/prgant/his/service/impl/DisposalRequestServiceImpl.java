package com.prgant.his.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prgant.his.mapper.DisposalRequestMapper;
import com.prgant.his.model.po.DisposalRequest;
import com.prgant.his.service.DisposalRequestService;
import org.springframework.stereotype.Service;

@Service
public class DisposalRequestServiceImpl extends ServiceImpl<DisposalRequestMapper, DisposalRequest>
        implements DisposalRequestService {

}
