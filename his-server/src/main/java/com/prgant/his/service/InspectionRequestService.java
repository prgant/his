package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.InspectionRequest;

public interface InspectionRequestService extends IService<InspectionRequest> {
}
