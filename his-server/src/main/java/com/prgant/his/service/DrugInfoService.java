package com.prgant.his.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.prgant.his.model.po.DrugInfo;

public interface DrugInfoService extends IService<DrugInfo> {
}
