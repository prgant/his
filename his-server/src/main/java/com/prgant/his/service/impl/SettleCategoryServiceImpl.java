package com.prgant.his.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prgant.his.mapper.SettleCategoryMapper;
import com.prgant.his.model.po.SettleCategory;
import com.prgant.his.service.SettleCategoryService;
import org.springframework.stereotype.Service;

@Service
public class SettleCategoryServiceImpl extends ServiceImpl<SettleCategoryMapper, SettleCategory> implements SettleCategoryService {
}
