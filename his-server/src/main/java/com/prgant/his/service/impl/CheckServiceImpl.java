package com.prgant.his.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.prgant.his.mapper.CheckRequestMapper;
import com.prgant.his.model.po.CheckRequest;
import com.prgant.his.service.CheckRequestService;
import org.springframework.stereotype.Service;

@Service
public class CheckServiceImpl extends ServiceImpl<CheckRequestMapper, CheckRequest> implements CheckRequestService {
}
